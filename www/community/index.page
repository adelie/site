<section class="section">
  <div class="container content">
    <h2 class="title is-2">Connect With Us</h2>
    <p>We warmly invite you to join our community of technology professionals.
    We're passionate about our ideals of reliability, security, portability, and usability,
    and about the libre software and hardware movements in general.</p>
    <div class="field is-grouped">
      <p class="control"><a class="button is-link is-medium" href="https://www.adelielinux.org/community/contribute.html">Get Involved &raquo;</a></p>
      <p class="control"><a class="button is-info is-medium" href="https://www.adelielinux.org/community/sponsors.html">Sponsors &raquo;</a></p>
    </div>
    <div class="box">
      <ol>
        <li><a href="#chat">Chat</a></li>
        <li><a href="#lists">Mailing Lists</a></li>
        <li><a href="#social">Social Media</a></li>
      </ol>
    </div>
  </div>
</section>
<section class="section">
  <div class="container content">
    <h3 id="chat" class="title is-3">Chat</h3>
    <div class="columns">
      <div class="column">
        <h4>Interlinked IRC</h4>
        <p>irc.interlinked.me &mdash; port 6697 or 9999, TLS only</p>
        <p>Anyone may join the #Adelie-Support room, for help with using Adélie;
        people who register with a valid email address can additionally join:</p>
        <ul>
          <li>#Adelie &mdash; development discussion</li>
          <li>#Adelie-Infra &mdash; AdelieLinux.org infrastructure discussion</li>
          <li>#Adelie-Social &mdash; general community chat room</li>
          <li>#horizon &mdash; discussion about Project Horizon</li>
        </ul>
      </div>
      <div class="column">
        <h4>Telegram</h4>
        <p>We have a social chatter channel at <a href="https://t.me/Adeliegram">Adéliegram</a>.
        Remember to follow our Code of Conduct, and have fun!</p>
      </div>
    </div>
  </div>
</section>
<section class="section">
  <div class="container content">
    <h3 id="lists" class="title is-3">Mailing Lists</h3>
    <p>Our mailing lists allow for more long-form discussions and assistance
    with the usage and development of Adélie Linux.  Anyone with a valid
    email address may subscribe to join the conversation.</p>
    <p>Please <a href="https://lists.adelielinux.org/archive/">click here</a> to access our lists.</p>
  </div>
</section>
<section class="section">
  <div class="container content">
    <h3 id="social" class="title is-3">Social Media</h3>
    <div class="columns">
      <div class="column">
        <h4>Mastodon / Pleroma / GNU/Social</h4>
        <a href="https://mst3k.interlinked.me/@AdelieLinux">@AdelieLinux@mst3k.interlinked.me</a>
      </div>
      <div class="column">
        <h4>Reddit</h4>
        <a href="https://www.reddit.com/r/AdelieLinux/">/r/AdelieLinux</a>
      </div>
      <div class="column">
        <h4>Twitter</h4>
        <a href="https://twitter.com/AdelieLinux">@AdelieLinux</a> <a href="https://twitter.com/AdelieLinux" class="twitter-follow-button" data-dnt="true" data-show-count="false">Follow Us on Twitter</a>
      </div>
    </div>
  </div>
</section>

<script async src="https://platform.twitter.com/widgets.js"></script>
