<section class="section">
  <div class="container">
    <p class="subtitle is-3">Adélie Linux is brought to you by the following people, and Users Like You.  Thank you.</p>
  </div>
</section>

<section class="section">
  <div class="container content">
    <h2>A. Wilcox (awilfox)</h2>
    <section class="media">
      <figure class="media-left image is-128x128">
        <img src="https://www.adelielinux.org/awilfox-avatar-square.jpg" alt="[Photo of awilfox@]" width="128" height="128">
      </figure>
      <div class="media-content">
        <dl>
          <dt>Social Links</dt>
          <dd>
            <a href="https://twitter.com/awilcox" class="twitter-follow-button" data-dnt="true" data-show-count="false">Follow @awilcox on Twitter</a>
            <a href="https://catfox.life/">Blog</a>
          </dd>
          <dt>Interests</dt>
          <dd>Usability; security; POSIX&reg; conformance; musl and toolchain development;
          multi-architecture porting; walking reference on x86 and PowerPC CPUs.</dd>
          <dt>Bio</dt>
          <dd>
            <p>Growing up, my grandfather introduced me to computer programming on his PC XT.
            My grandmother was a computer programmer at a university, and my mother was a Perl scripter
            for our church's Web site in the 1990s.  I've always been around technology and my family's
            wonderful legacy of nerdiness lives on in me.</p>
            <p>I have a passion for using technology to solve problems, and thoroughly enjoy helping others.
            I'm at my happiest when these combine, and I'm able to help people through software I've written.
            In my spare time I like to listen to EDM and jazz, play the piano and guitar, read books,
            and spend time with good friends and family.</p>
          </dd>
        </dl>
      </div>
    </section>

    <hr>

    <h2>Brandon Bergren (Bdragon)</h2>
    <section class="media">
      <figure class="media-left image is-128x128">
        &nbsp;
      </figure>
      <div class="media-content">
        <dl>
          <dt>Interests</dt>
          <dd>PowerPC port; toolchains; Python 3.</dd>
        </dl>
      </div>
    </section>

    <hr>

    <h2>Dan Theisen (djt)</h2>
    <section class="media">
      <figure class="media-left image is-128x128">
        &nbsp;
      </figure>
      <div class="media-content">
        <dl>
          <dt>Interests</dt>
          <dd>Networking; email; lightweight X11 window managers.</dd>
        </dl>
      </div>
    </section>

    <hr>

    <h2>Horst Burkhardt (mc680x0)</h2>
    <section class="media">
      <figure class="media-left image is-128x128">
        <img src="https://www.adelielinux.org/horst-avatar.jpg" alt="[Photo of mc680x0@]" width="128" height="128">
      </figure>
      <div class="media-content">
        <dl>
          <dt>Social Links</dt>
          <dd>
            <a href="https://twitter.com/HorstBurkhardt" class="twitter-follow-button" data-dnt="true" data-show-count="false">Follow @HorstBurkhardt</a>
          </dd>
          <dt>Interests</dt>
          <dd>Linux kernel (maintains easy-kernel); PowerPC port; games; Emacs; photography; m68k architecture.</dd>
          <dt>Bio</dt>
          <dd>
            Growing up, a recurring theme was "making old things work again". From the
            derelicted Apple II in my kindergarten classroom, to old LCs and Colour Classics
            at my high school, the various cameras I collect, and the clocks I occasionally
            repair. Now, working with my fellow Adélie contributors, I can make yet more
            mechanisms reach the peak of their performance.
          </dd>
        </dl>
      </div>
    </section>

    <hr>

    <h2>Kiyoshi Aman (Aerdan)</h2>
    <section class="media">
      <figure class="media-left image is-128x128">
        <img src="https://www.adelielinux.org/aerdan-avatar.png" alt="[Avatar of aerdan@]" width="128" height="128">
      </figure>
      <div class="media-content">
        <dl>
          <dt>Social Links</dt>
          <dd>
            <a href="https://twitter.com/aerdan" class="twitter-follow-button" data-dnt="true" data-show-count="false">Follow @aerdan on Twitter</a>
            <a href="https://pleroma.apkfission.net/aerdan">Follow @aerdan on pleroma.apkfission.net</a>
          </dd>
          <dt>Interests</dt>
          <dd>Desktops (maintains LXQt and XFCE); Perl; Python 3.</dd>
        </dl>
      </div>
    </section>

    <hr>

    <h2>Laurent Bercot (skarnet)</h2>
    <section class="media">
      <figure class="media-left image is-128x128">
        &nbsp;
      </figure>
      <div class="media-content">
        <dl>
          <dt>Interests</dt>
          <dd>Creating the s6 service manager and s6-linux-init package; qmail.</dd>
        </dl>
      </div>
    </section>

    <hr>

    <h2>Lee Starnes (raven)</h2>
    <section class="media">
      <figure class="media-left image is-128x128">
        &nbsp;
      </figure>
      <div class="ym-gbox">
        <dl>
          <dt>Interests</dt>
          <dd>Founding member of Community Arbitration; VPNs.</dd>
        </dl>
      </div>
    </section>

    <hr>

    <h2>Max Rees ([[sroracle]])</h2>
    <section class="media">
      <figure class="media-left image is-128x128">
        &nbsp;
      </figure>
      <div class="ym-gbox">
        <dl>
          <dt>Interests</dt>
          <dd>Security; creating APK Foundry; science and mathematics; productivity software.</dd>
        </dl>
      </div>
    </section>

    <hr>

    <h2>Samuel Holland (smaeul)</h2>
    <section class="media">
      <figure class="media-left image is-128x128">
        &nbsp;
      </figure>
      <div class="ym-gbox">
        <dl>
          <dt>Interests</dt>
          <dd>The Rust programming language; PowerPC; ARM, with a specialisation in sunxi SoCs.</dd>
        </dl>
      </div>
    </section>
  </div>
</section>

<script async src="https://platform.twitter.com/widgets.js"></script>
