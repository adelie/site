<section class="hero">
  <div class="hero-body">
    <div class="container">
      <h1 class="title is-2">Open collaboration helps us all.</h1>
      <p>At Adélie, contributing back to the Libre Software community is second nature.
      We regularly contribute to projects and initiatives that share our goals,
      and we're happy to introduce the community to our own sub-projects.</p>
    </div>
  </div>
</section>
<section class="section">
  <div class="container">
    <h2 class="title is-3">Our Projects</h2>
    <div class="columns">

      <div class="column">
        <div class="card">
          <div class="card-image">
            <figure class="image is-1by1">
              <a href="https://gcompat.com/">
                <img src="https://www.adelielinux.org/img/gcompat.svg" alt="gcompat">
              </a>
            </figure>
          </div>
          <div class="card-content">
            <h4 class="title is-4"><a href="https://gcompat.com/">gcompat</a></h4>
            <p><strong>gcompat</strong> is a project started by Adélie Linux
            to allow users to run binaries compiled for glibc distributions
            on musl distributions, such as Adélie.  gcompat serves an important
            role to all musl distributions, allowing users to utilise their
            existing Linux software while retaining the benefits a musl-based
            distribution offers.</p>
          </div>
          <footer class="card-footer">
            <a class="card-footer-item" href="https://gcompat.com/">Go to Site &raquo;</a>
            <a class="card-footer-item" href="https://git.adelielinux.org/adelie/gcompat/">View Git &raquo;</a>
          </footer>
        </div>
      </div>

      <div class="column">
        <div class="card">
          <div class="card-image">
            <figure class="image is-1by1">
              <a href="https://www.adelielinux.org/horizon/">
                <img src="https://www.adelielinux.org/img/horizon.svg" alt="Horizon">
              </a>
            </figure>
          </div>
          <div class="card-content">
            <h4 class="title is-4"><a href="https://www.adelielinux.org/horizon/">Horizon</a></h4>
            <p><strong>Horizon</strong> is the name of the Adélie Linux
            installation system project.  It is designed to ensure that users
            are able to complete the installation process with ease and
            understanding, and that systems administrators are able to quickly
            and effortlessly deploy Adélie, whether to one computer or one
            thousand computers.</p>
          </div>
          <footer class="card-footer">
            <a class="card-footer-item" href="https://www.adelielinux.org/horizon/">Go to Site &raquo;</a>
            <a class="card-footer-item" href="https://git.adelielinux.org/adelie/horizon/">View Git &raquo;</a>
          </footer>
        </div>
      </div>

      <div class="column">
        <div class="card">
          <div class="card-image">
            <figure class="image is-1by1">
              <a href="https://git.adelielinux.org/adelie/shimmy/">
                <img src="https://www.adelielinux.org/img/shimmy.svg" alt="Shimmy">
              </a>
            </figure>
          </div>
          <div class="card-content">
            <h4 class="title is-4"><a href="https://git.adelielinux.org/adelie/shimmy/">Shimmy</a></h4>
            <p><strong>Shimmy</strong> is a collection of command-line
            utilities designed to provide stricter POSIX&reg; conformance
            to Linux and other libre software Unix-like operating
            environments.  It has been engineered to be portable, fast,
            and conformant.  Shimmy provides the <code>getconf</code>
            command, and others, in the default Adélie installation.</p>
          </div>
          <footer class="card-footer">
            <a class="card-footer-item" href="https://git.adelielinux.org/adelie/shimmy/">View Git &raquo;</a>
          </footer>
        </div>
    </div>
  </div>
</section>
